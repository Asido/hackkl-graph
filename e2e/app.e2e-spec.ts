import { HackklGraphPage } from './app.po';

describe('hackkl-graph App', () => {
  let page: HackklGraphPage;

  beforeEach(() => {
    page = new HackklGraphPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
