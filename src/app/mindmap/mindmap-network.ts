import * as vis from "vendor/vis-network.min.js";
import "vendor/vis-network.min.css";

declare var vis: any;

export class MindmapNetwork {
    private static id = 0;

    private readonly nodes = new vis.DataSet();
    private readonly edges = new vis.DataSet();

    private readonly network: any;

    private challengeNodeId: string = null;

    constructor(canvas: HTMLElement) {
        this.challengeNodeId = this.nextId();
        this.nodes.add({
            id: this.challengeNodeId,
            shape: "hexagon",
            color: "#e6bfd3",
            size: 50
        });

        const data = {
            nodes: this.nodes,
            edges: this.edges
        };

        const options = {};

        this.network = new vis.Network(canvas, data, options);
    }

    public addIdea(): void {
        const nodeId = this.nextId();

        this.nodes.add({
            id: nodeId,
            shape: "diamond",
            color: "#e9499d"
        });

        this.edges.add({
            id: this.nextId(),
            from: this.getNewNodeParentId(),
            to: nodeId,
            width: 2,
            color: {
                inherit: false
            },
            hoverWidth: 0,
            selectionWidth: 0
        });

        this.network.fit({ animation: true });
    }

    public addResearch(): void {
        const nodeId = this.nextId();

        this.nodes.add({
            id: nodeId,
            shape: "circle"
        });

        this.edges.add({
            id: this.nextId(),
            from: this.getNewNodeParentId(),
            to: nodeId,
            width: 2,
            color: {
                inherit: false
            },
            hoverWidth: 0,
            selectionWidth: 0
        });

        this.network.fit({ animation: true });        
    }

    private nextId(): string {
        return (MindmapNetwork.id++).toString();
    }

    private getNewNodeParentId(): string {
        const selectedNodes = this.network.getSelectedNodes();
        if (selectedNodes.length) return selectedNodes[0];
        return this.challengeNodeId;
    }
}
