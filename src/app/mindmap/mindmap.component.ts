import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from "@angular/core";
import { MindmapNetwork } from "./mindmap-network";

@Component({
    selector: "hk-mindmap",
  templateUrl: "./mindmap.component.html",
  styleUrls: ["./mindmap.component.css"]
})
export class MindmapComponent implements AfterViewInit {
    @ViewChild("mindmap") mindmapRef: ElementRef;

    private mindmap: MindmapNetwork;

    ngAfterViewInit() {
        this.mindmap = new MindmapNetwork(this.mindmapRef.nativeElement);
        // this.mindmap.challengeTitle = "Test challenge";
    }

    public onAddIdea(): void {
        this.mindmap.addIdea();
    }

    public onAddResearch(): void {
        this.mindmap.addResearch();
    }
}
