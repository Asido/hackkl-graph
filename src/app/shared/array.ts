interface Array<T> {
    hkDeleteIndex(index: number): T;
    hkDeleteValue(value: T): boolean;
}

Array.prototype.hkDeleteIndex = function (index: number) {
    return this.splice(index, 1)[0];
};

Array.prototype.hkDeleteValue = function <T>(value: T) {
    const idx = this.indexOf(value);

    if (idx !== -1) {
        this.hkDeleteIndex(idx);
        return true;
    }

    return false;
}
