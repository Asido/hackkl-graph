export class Random {
    public static bool(): boolean {
        return Math.random() >= 0.5;
    }

    public static int(min: number = 0, max: number = 2147483647): number {
        return (Math.random() * (max - min + 1) + min) | 0;
    }
}
